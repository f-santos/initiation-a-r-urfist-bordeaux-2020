# Inspection, importation et contrôle des données

1.  Plusieurs formats sont disponibles, mais le fichier doit préférentiellement être téléchargé au format CSV. Une inspection rapide du fichier avec un éditeur de texte (tel que Notepad++, installé sur les postes de la salle de formation) montre que son séparateur décimal est le point, et que son séparateur de champ est la virgule.
2.  Une astuce : les données peuvent être importées directement depuis un site web, sans passer par la case &laquo;&nbsp;téléchargement sur le disque dur&nbsp;&raquo;. Un exemple ici :
    
    ```R
    dat <- read.csv("http://web.utk.edu/~auerbach/Goldman.csv",
                    header = TRUE,
                    sep = ",", dec = ".",
                    stringsAsFactors = TRUE,
                    na.strings = "",
                    fileEncoding = "macintosh")
    ```

3.  On vérifie que le nombre d&rsquo;individus est conforme au nombre d&rsquo;individus annoncé sur le site :
    
    ```R
    dim(dat)
    ```
    
        [1] 1538   69
    
    De plus, d&rsquo;autres contrôles peuvent (ou doivent) être menés :
    
    ```R
    str(dat)
    summary(dat)
    ```


# Filtrage et mise en forme des données

1.  Pour ne retenir que certaines variables, le plus rapide est d&rsquo;utiliser la syntaxe simplifiée provenant du package `dplyr`.
    
    ```R
    library(dplyr)
    dat <- select(dat, ID, Sex, NOTE, LHML, LHHD, LHAPD, LHMLD,
                  LFML, LFHD, LRMLD, LRAPD)
    head(dat)
    ```
    
        	ID Sex                     NOTE  LHML  LHHD LHAPD
        1  99.1/69   0  Tigara - Point Hope, AK 308.5 47.55 22.00
        2  99.1/70   1  Norton - Point Hope, AK    NA    NA    NA
        3 99.1/75A   0 Ipituaq - Point Hope, AK 311.0 44.44 22.12
        4  99.1/80   0 Ipituaq - Point Hope, AK 289.0 42.94 20.36
        5 99.1/83A   1 Ipituaq - Point Hope, AK 295.0 42.51 19.35
        6 99.1/86B   1 Ipituaq - Point Hope, AK 270.5 39.74 19.42
          LHMLD LFML  LFHD LRMLD LRAPD
        1 24.30  443 44.92 14.84 12.02
        2    NA  386 44.64    NA    NA
        3 22.20  415 43.95    NA    NA
        4 20.83  398 43.97 13.89 11.12
        5 19.34  395 40.58 14.38  9.62
        6 19.22   NA    NA 13.66  8.85
    
    Notons que cela *écrase* le jeu de données d&rsquo;origine, dont on ne possède désormais plus aucune instance en mémoire. Selon la situation, cela peut être, ou ne pas être, un choix pertinent.
    
    *Remarque*. &#x2014; Ceci était parfaitement équivalent à la syntaxe R de base suivante :
    
    ```R
    dat <- dat[ , c("ID", "Sex", "NOTE", "LHML", "LHHD", "LHAPD",
                    "LHMLD", "LFML", "LFHD", "LRMLD", "LRAPD")]
    ```

2.  Pour filtrer les trois populations d&rsquo;intérêt :
    
    ```R
    dat <- subset(dat, NOTE %in% c("Cliff Dweller",
                                   "Hainburg",
                                   "Indian Knoll"))
    ```
    
    Cela nous laisse désormais seulement 133 individus :
    
    ```R
    dim(dat)
    ```
    
        [1] 133  11
    
    Une fois de plus, cela écrase le jeu de données `dat`.

3.  La variable `Sex` possède effectivement un codage peu explicite :
    
    ```R
    summary(dat$Sex) # ou table(dat$Sex), qui est équivalent
    ```
    
         0 0?  1 1? 
        73  4 54  2
    
    La documentation en ligne du Goldman Data Set indique qu&rsquo;en réalité, les `0` sont des hommes, les `1` sont des femmes, et le reste correspond à des estimations incertaines du sexe des individus. On souhaite recoder ce facteur afin de le rendre plus explicite, et de supprimer les modalités `0?` et `1?` pour les remplacer par des valeurs manquantes (`NA`). Plusieurs approches, plusieurs syntaxes, sont envisageables pour réaliser cette opération.
    
    Avec les fonctionnalités R de base :
    
    ```R
    levels(dat$Sex) <- c("Male", NA, "Female", NA)
    ```
    
    Ou, peut-être plus explicitement, avec les fonctionnalités du package `forcats` :
    
    ```R
    library(forcats)
    dat$Sex <- fct_recode(dat$Sex,
                          Male = "0",
                          Female = "1",
                          NULL = "0?",
                          NULL = "1?")
    ```

4.  On constate que la variable `NOTE` possède encore en mémoire de nombreuses modalités inutilisées :
    
    ```R
    summary(dat$NOTE, maxsum = 10)
    ```
    
        				  Indian Knoll 
        					    61 
        				      Hainburg 
        					    37 
        				 Cliff Dweller 
        					    35 
        "Arancano, Necropolis di.  Ancient Lima, Peru" 
        					     0 
        				     "Bushman" 
        					     0 
        	"Cape York - Australia Settentriouale" 
        					     0 
               "Clarence River" (Nouva Galles del Sud) 
        					     0 
        		      "Indian from California" 
        					     0 
        		       "La Sociedad" Australia 
        					     0 
        				       (Other) 
        					     0
    
    Après le filtrage des populations précédemment réalisé, il n&rsquo;y a effectivement plus aucun individu Bushman, ou Péruvien, etc., mais R garde en mémoire que ces modalités-là ont un jour existé et ont contenu des individus. Cela n&rsquo;est pas toujours gênant, mais si on souhaite se débarrasser définitivement de ces modalités :
    
    ```R
    dat <- droplevels(dat)
    ```


# Analyse de la structure de corrélation

1.  Pour calculer la matrice des corrélations, on commence par extraire uniquement les variables numériques du jeu de données :
    
    ```R
    num <- select_if(dat, is.numeric)
    head(num)                       
    ```
    
             LHML  LHHD LHAPD LHMLD  LFML  LFHD LRMLD LRAPD
        163    NA    NA    NA    NA 440.5 44.50    NA    NA
        164 323.0 43.97 17.85 19.99    NA    NA 13.64 11.63
        165    NA    NA    NA    NA 424.0 41.44 12.00 10.30
        166 294.0 39.17 18.89 17.85 416.5 40.75 13.78 10.46
        167 290.5 36.52 16.50 18.20 405.5 39.78 11.61  9.65
        168 287.5 40.43 17.15 19.56 421.0 38.08 13.35  9.58
    
    On peut désormais calculer et afficher la matrice des corrélations entre toutes ces variables :
    
    ```R
    mat <- cor(num, use = "complete.obs")
    print(mat)
    ```
    
        	   LHML      LHHD     LHAPD     LHMLD      LFML
        LHML  1.0000000 0.7153923 0.3499274 0.3823247 0.8702572
        LHHD  0.7153923 1.0000000 0.5406203 0.5850014 0.7162976
        LHAPD 0.3499274 0.5406203 1.0000000 0.7708735 0.4708201
        LHMLD 0.3823247 0.5850014 0.7708735 1.0000000 0.4837539
        LFML  0.8702572 0.7162976 0.4708201 0.4837539 1.0000000
        LFHD  0.6877975 0.8642789 0.6262210 0.6100942 0.7697402
        LRMLD 0.2475683 0.5128618 0.7623024 0.7165613 0.3632046
        LRAPD 0.5602411 0.5984860 0.6816035 0.5997114 0.5072506
        	   LFHD     LRMLD     LRAPD
        LHML  0.6877975 0.2475683 0.5602411
        LHHD  0.8642789 0.5128618 0.5984860
        LHAPD 0.6262210 0.7623024 0.6816035
        LHMLD 0.6100942 0.7165613 0.5997114
        LFML  0.7697402 0.3632046 0.5072506
        LFHD  1.0000000 0.5428547 0.6212784
        LRMLD 0.5428547 1.0000000 0.6301524
        LRAPD 0.6212784 0.6301524 1.0000000
    
    Dès que le nombre de variables augmente, ce genre de matrices devient difficilement lisible à l&rsquo;oeil nu, et des méthodes graphiques de visualisation des corrélations deviennent préférables.

2.  Après lecture de la vignette de `corrplot`, on peut par exemple tenter la représentation graphique suivante :
    
    ```R
    library(corrplot)
    corrplot(mat, method = "circle", order = "hclust", addrect = 2)
    ```
    
    ![img](mat_cor.png "Visualisation de la matrice de corrélation avec `corrplot`.")
    
    Bien que ce ne soit pas totalement spectaculaire, deux blocs de variables semblent pouvoir être identifiés : le premier bloc inclut les variables mesurant des longueurs totales et des têtes osseuses, le second bloc inclut des variables mesurant des diamètres osseux. On peut donc trouver une certaine logique ostéométrique dans ces regroupements.

3.  Pour effectuer le nuage de points demandé, utiliser `lattice` est la solution la plus économe en lignes de code :
    
    ```R
    library(lattice)
    xyplot(LFML ~ LHML | NOTE, groups = Sex, data = dat,
           auto.key = TRUE)
    ```
    
    ![img](xyplot_lattice.png "Un nuage de points généré avec `lattice`.")
    
    Produire un graphique équivalent avec les fonctions R de base nécessite un peu plus d&rsquo;efforts :
    
    ```R
    ## Éclater le dataframe selon la colonne NOTE :
    sp <- split(dat, dat$NOTE)
    ## Séparer la fenêtre graphique en trois panneaux : 
    par(mfrow = c(1, 3), cex = 0.9)
    ## Remplir chaque panneau avec un groupe précis :
    for (k in 1:length(sp)) {
        plot(LFML ~ LHML, data = sp[[k]], pch = 16, col = Sex,
             xlim = c(270, 360), main = names(sp)[k])
        legend("topleft", pch = 16, col = 1:2,
               legend = levels(dat$Sex))
    }
    ```
    
    ![img](biplot_Rbase.png "Un équivalent avec R-base.")
    
    *Remarque*. &#x2014; Cet exemple est un prétexte pour montrer l&rsquo;utilisation de boucles `for` en langage R. Ces boucles sont généralement déconseillés, car elles s&rsquo;exécutent assez lentement, et ne correspondent pas bien à la philosophie très vectorisée du langage R. Un idiome typique du langage R serait plutôt basé sur des fonctions du type `*apply`, mais cela nécessiterait de savoir définir une fonction personnalisée en R, ce qui ne sera introduit que plus loin dans le TP.


# Dimorphisme sexuel pour le fémur à Indian Knoll

1.  Concentrons-nous à présent sur la population d&rsquo;Indian Knoll :
    
    ```R
    ik <- subset(dat, NOTE == "Indian Knoll")
    ```
    
    Cela nous laisse 58 individus, répartis comme suit en fonction du sexe :
    
    ```R
    table(ik$Sex)
    ```
    
        Male Female 
          29     29

2.  Dans ce qui suit, on va travailler sur le ratio entre deux mesures du fémur gauche :
    
    ```R
    ik$Ratio <- ik$LFML / ik$LFHD     
    ```

3.  Étudions la normalité de ce ratio, à la fois graphiquement et avec des tests de normalité.
    
    ```R
    densityplot(~ Ratio | Sex, data = ik)
    ```
    
    ![img](norm_ratio.png "Estimation de densité par méthode à noyau pour le `Ratio` fémoral des hommes et des femmes dans l&rsquo;échantillon d&rsquo;Indian Knoll.")
    
    Au vu des densités estimées, la normalité semble être une hypothèse plausible pour les deux groupes, comme le confirment des tests de normalité de Shapiro-Wilk :
    
    ```R
    tapply(ik$Ratio, INDEX = ik$Sex, FUN = shapiro.test)
    ```
    
        $Male
        
        	Shapiro-Wilk normality test
        
        data:  X[[i]]
        W = 0.96869, p-value = 0.5248
        
        
        $Female
        
        	Shapiro-Wilk normality test
        
        data:  X[[i]]
        W = 0.95642, p-value = 0.2676

4.  Le niveau moyen du `Ratio` fémoral peut ensuite être comparé entre les hommes et les femmes, d&rsquo;abord graphiquement, puis avec un *t*-test (possible puisque l&rsquo;hypothèse de normalité semble acceptable ici).
    
    ```R
    boxplot(Ratio ~ Sex, data = ik)
    ```
    
    ![img](boxplots_ratio.png "Boîtes de dispersion comparant le Ratio fémoral des femmes et des hommes pour la population d&rsquo;Indian Knoll.")

Un *t*-test, et surtout l&rsquo;intervalle de confiance associé, peut permettre d&rsquo;inférer sur cette différence de niveau moyen :

```R
t.test(Ratio ~ Sex, data = ik)
```

    
    	Welch Two Sample t-test
    
    data:  Ratio by Sex
    t = -2.7766, df = 55.988, p-value = 0.007456
    alternative hypothesis: true difference in means is not equal to 0
    95 percent confidence interval:
     -0.57696623 -0.09335395
    sample estimates:
      mean in group Male mean in group Female 
    	    10.31103             10.64619

Avec une *p*-valeur très faible, ainsi qu&rsquo;un intervalle de confiance à 95% d&rsquo;environ \([-0.577 ; -0.093]\) pour la différence entre les moyennes (intervalle qui en particulier n&rsquo;inclut pas la valeur 0), les données semblent incompatibles avec l&rsquo;hypothèse nulle d&rsquo;égalité populationnelle des moyennes des femmes et des hommes pour ce `Ratio` fémoral.


# Questions bonus diverses

1.  Plusieurs approches possibles ici. La première méthode consisterait tout simplement en une représentation graphique :
    
    ```R
    library(car)
    Boxplot(dat$LHML)
    ```
    
    ![img](boxplot_LHML.png "Boîte de dispersion pour la variable `LHML`.")
    
    Ici, il se trouve que la valeur maximale de la variable `LHML` est également un *outlier* pour cette variable, ce qui permet son identification avec une `Boxplot()`. Toutefois, cela ne sera pas toujours le cas, et cette approche n&rsquo;est donc pas généralisable.
    
    Une approche plus sûre consiste à utiliser la fonction `which.max()`, qui, comme son nom (ou sa page d&rsquo;aide !) le laisse entendre, permet de repérer l&rsquo;index de l&rsquo;individu qui obtient la valeur maximale dans un vecteur donné :
    
    ```R
    which.max(dat$LHML)
    ```
    
        [1] 100
